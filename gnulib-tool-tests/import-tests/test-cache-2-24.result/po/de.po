# German translation of gnulib messages.
# Copyright © 2011 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnulib package.
# Karl Eichwalder <ke@suse.de>, 2001-2002.
# Lutz Behnke <lutz.behnke@gmx.de>, 1996, 1997, 1998, 1999, 2000, 2001.
# Michael Schmidt <michael@guug.de>, 1996, 1997, 1998, 1999, 2000.
# Michael Piefel <piefel@informatik.hu-berlin.de>, 2001, 2002, 2003, 2009.
# Kai Wasserbäch <debian@carbon-project.org>, 2009.
# Arun Persaud <arun@nubati.net>, 2012.
# Roland Illig <roland.illig@gmx.de>, 2019.
#
# TAB: spell it out („Tabulatoren“). -ke-
# Don't use obscure abbreviations, please.  -ke-
# No hyphenation, please. -ke-
#
# space: Leerzeichen oder Leerschritt
#
# Check:
# idle - untätig
#   idle: untätig, ruhig, „idle“, Leerlauf
#   user idle time: Untätigkeitszeit des Benutzers, Ruhezeit, Idle-Time,
#                   Benutzer im Leerlauf
# digit - Zahl, Ziffer, Nummer, Stelle
# logged in - angemeldet, eingeloggt
# requested - gewünscht?
#
# Some comments on translations used in oder to ensure persistence:
#
# symbolic links: symbolische Verknüpfungen
# hard links:     harte Verknüpfungen
# backup:         Sicherung
# mount:          einhängen
#
msgid ""
msgstr ""
"Project-Id-Version: GNU gnulib-4.0.0.2567\n"
"Report-Msgid-Bugs-To: bug-gnulib@gnu.org\n"
"POT-Creation-Date: 2019-05-18 19:06+0200\n"
"PO-Revision-Date: 2019-05-22 20:00+0200\n"
"Last-Translator: Roland Illig <roland.illig@gmx.de>\n"
"Language-Team: German <translation-team-de@lists.sourceforge.net>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.3\n"

#: lib/argmatch.c:134
#, c-format
msgid "invalid argument %s for %s"
msgstr "ungültiges Argument %s für %s"

#: lib/argmatch.c:135
#, c-format
msgid "ambiguous argument %s for %s"
msgstr "mehrdeutiges Argument %s für %s"

#: lib/argmatch.c:154
msgid "Valid arguments are:"
msgstr "Gültige Argumente sind:"

#: lib/argp-help.c:148
#, c-format
msgid "ARGP_HELP_FMT: %s value is less than or equal to %s"
msgstr "ARGP_HELP_FMT: Der Wert %s ist kleiner oder gleich %s"

#: lib/argp-help.c:224
#, c-format
msgid "%.*s: ARGP_HELP_FMT parameter requires a value"
msgstr "%.*s: ARGP_HELP_FMT Parameter benötigt einen Wert"

#: lib/argp-help.c:234
#, c-format
msgid "%.*s: Unknown ARGP_HELP_FMT parameter"
msgstr "%.*s: Unbekannter Parameter für ARGP_HELP_FMT"

#: lib/argp-help.c:247
#, c-format
msgid "Garbage in ARGP_HELP_FMT: %s"
msgstr "Müll in ARGP_HELP_FMT: %s"

#: lib/argp-help.c:1228
msgid "Mandatory or optional arguments to long options are also mandatory or optional for any corresponding short options."
msgstr "Erforderliche oder optionale Argumente für lange Optionen sind auch für kurze erforderlich bzw. optional."

#: lib/argp-help.c:1617
msgid "Usage:"
msgstr "Aufruf:"

#: lib/argp-help.c:1621
msgid "  or: "
msgstr "  oder: "

#: lib/argp-help.c:1633
msgid " [OPTION...]"
msgstr " [OPTION…]"

#: lib/argp-help.c:1660
#, c-format
msgid "Try '%s --help' or '%s --usage' for more information.\n"
msgstr "»%s --help« oder »%s --usage« liefert weitere Informationen.\n"

#: lib/argp-help.c:1688
#, c-format
msgid "Report bugs to %s.\n"
msgstr "Melden Sie Fehler (auf Englisch, mit LC_ALL=C) an <%s>.\n"

#: lib/argp-help.c:1889 lib/error.c:195
msgid "Unknown system error"
msgstr "Unbekannter Systemfehler"

#: lib/argp-parse.c:81
msgid "give this help list"
msgstr "diese Hilfeliste anzeigen"

#: lib/argp-parse.c:82
msgid "give a short usage message"
msgstr "eine Kurzfassung des Aufrufs anzeigen"

#: lib/argp-parse.c:83
msgid "NAME"
msgstr "NAME"

#: lib/argp-parse.c:84
msgid "set the program name"
msgstr "den Programmnamen festlegen"

#: lib/argp-parse.c:85
msgid "SECS"
msgstr "SEK"

#: lib/argp-parse.c:86
msgid "hang for SECS seconds (default 3600)"
msgstr "SEK Sekunden warten (Standardwert 3600)"

#: lib/argp-parse.c:143
msgid "print program version"
msgstr "Programmversion anzeigen"

#: lib/argp-parse.c:160
msgid "(PROGRAM ERROR) No version known!?"
msgstr "(PROGRAMMFEHLER) Keine Version bekannt!?"

#: lib/argp-parse.c:613
#, c-format
msgid "%s: Too many arguments\n"
msgstr "%s: zu viele Argumente\n"

#: lib/argp-parse.c:759
msgid "(PROGRAM ERROR) Option should have been recognized!?"
msgstr "(PROGRAMMFEHLER) Option hätte erkannt werden müssen!?"

#: lib/bitset/stats.c:177
#, c-format
msgid "%u bitset_allocs, %u freed (%.2f%%).\n"
msgstr "%u bitset_allocs, %u freigegeben (%.2f%%).\n"

#: lib/bitset/stats.c:180
#, c-format
msgid "%u bitset_sets, %u cached (%.2f%%)\n"
msgstr "%u bitset_sets, %u gecacht (%.2f%%)\n"

#: lib/bitset/stats.c:183
#, c-format
msgid "%u bitset_resets, %u cached (%.2f%%)\n"
msgstr "%u bitset_resets, %u gecacht (%.2f%%)\n"

#: lib/bitset/stats.c:186
#, c-format
msgid "%u bitset_tests, %u cached (%.2f%%)\n"
msgstr "%u bitset_tests, %u gecacht (%.2f%%)\n"

#: lib/bitset/stats.c:190
#, c-format
msgid "%u bitset_lists\n"
msgstr "%u bitset_lists\n"

#: lib/bitset/stats.c:192
msgid "count log histogram\n"
msgstr "logarithmisches Anzahlhistogramm\n"

#: lib/bitset/stats.c:195
msgid "size log histogram\n"
msgstr "logarithmisches Größenhistogramm\n"

#: lib/bitset/stats.c:198
msgid "density histogram\n"
msgstr "Dichtehistogramm\n"

#: lib/bitset/stats.c:210
#, c-format
msgid ""
"Bitset statistics:\n"
"\n"
msgstr ""
"Bitmengen-Statistik:\n"
"\n"

#: lib/bitset/stats.c:213
#, c-format
msgid "Accumulated runs = %u\n"
msgstr "Aufsummierte Durchläufe = %u\n"

#: lib/bitset/stats.c:255 lib/bitset/stats.c:260
msgid "cannot read stats file"
msgstr "Statistikdatei konnte nicht gelesen werden"

#: lib/bitset/stats.c:257
#, c-format
msgid "bad stats file size\n"
msgstr "Statistikdatei hat falsche Größe\n"

#: lib/bitset/stats.c:281 lib/bitset/stats.c:283
msgid "cannot write stats file"
msgstr "Statistikdatei konnte nicht angelegt werden"

#: lib/bitset/stats.c:286
msgid "cannot open stats file for writing"
msgstr "Statistikdatei konnte nicht zum Schreiben geöffnet werden"

#: lib/c-stack.c:211 lib/c-stack.c:304
msgid "program error"
msgstr "Programmfehler"

#: lib/c-stack.c:212 lib/c-stack.c:305
msgid "stack overflow"
msgstr "Stacküberlauf"

#: lib/clean-temp.c:322
msgid "cannot find a temporary directory, try setting $TMPDIR"
msgstr "kein temporäres Verzeichnis gefunden, versuchen Sie, $TMPDIR zu setzen"

#: lib/clean-temp.c:336
#, c-format
msgid "cannot create a temporary directory using template \"%s\""
msgstr "temporäres Verzeichnis mit der Schablone »%s« konnte nicht angelegt werden"

#: lib/clean-temp.c:432
#, c-format
msgid "cannot remove temporary file %s"
msgstr "temporäre Datei »%s« konnte nicht entfernt werden"

#: lib/clean-temp.c:447
#, c-format
msgid "cannot remove temporary directory %s"
msgstr "temporäres Verzeichnis »%s« konnte nicht entfernt werden"

#: lib/closein.c:100
msgid "error closing file"
msgstr "Fehler beim Schließen der Datei"

#: lib/closeout.c:122
msgid "write error"
msgstr "Fehler beim Schreiben der Datei"

#: lib/copy-acl.c:54 lib/copy-file.c:198
#, c-format
msgid "preserving permissions for %s"
msgstr "Zugriffsberechtigungen von »%s« werden beibehalten"

#: lib/copy-file.c:175
#, c-format
msgid "error while opening %s for reading"
msgstr "die Datei »%s« konnte nicht zum Lesen geöffnet werden"

#: lib/copy-file.c:179
#, c-format
msgid "cannot open backup file %s for writing"
msgstr "die Sicherungsdatei »%s« konnte nicht zum Schreiben geöffnet werden"

#: lib/copy-file.c:183
#, c-format
msgid "error reading %s"
msgstr "Fehler beim Lesen von »%s«"

#: lib/copy-file.c:187
#, c-format
msgid "error writing %s"
msgstr "Fehler beim Schreiben von »%s«"

#: lib/copy-file.c:191
#, c-format
msgid "error after reading %s"
msgstr "Fehler nach dem Lesen von »%s«"

#: lib/csharpcomp.c:202 lib/javaversion.c:78
msgid "fdopen() failed"
msgstr "Fehler bei fdopen()"

#: lib/csharpcomp.c:443
msgid "C# compiler not found, try installing mono"
msgstr "C#-Compiler nicht gefunden, versuchen Sie, das Paket »mono« zu installieren"

#: lib/csharpexec.c:255
msgid "C# virtual machine not found, try installing mono"
msgstr "Virtuelle Maschine für C# nicht gefunden, versuchen Sie, das Paket »mono« zu installieren"

#: lib/dfa.c:970
msgid "unbalanced ["
msgstr "öffnende eckige Klammer »[« ohne Gegenstück"

#: lib/dfa.c:1091
msgid "invalid character class"
msgstr "ungültige Zeichenklasse"

#: lib/dfa.c:1217
msgid "character class syntax is [[:space:]], not [:space:]"
msgstr "Die Schreibweise für Zeichenklassen ist [[:space:]], nicht [:space:]"

#: lib/dfa.c:1284
msgid "unfinished \\ escape"
msgstr "unvollendete \\-Escapesequenz"

#: lib/dfa.c:1445
msgid "invalid content of \\{\\}"
msgstr "Ungültiger Inhalt in \\{\\}"

#: lib/dfa.c:1448
msgid "regular expression too big"
msgstr "der reguläre Ausdruck ist zu groß"

#: lib/dfa.c:1863
msgid "unbalanced ("
msgstr "öffnende Klammer »(« ohne Gegenstück"

#: lib/dfa.c:1981
msgid "no syntax specified"
msgstr "keine Syntax angegeben"

#: lib/dfa.c:1992
msgid "unbalanced )"
msgstr "schließende Klammer »)« ohne Gegenstück"

#: lib/execute.c:185 lib/execute.c:258 lib/spawn-pipe.c:235
#: lib/spawn-pipe.c:349 lib/wait-process.c:290 lib/wait-process.c:364
#, c-format
msgid "%s subprocess failed"
msgstr "%s: Unterprozess fehlgeschlagen"

#: lib/file-type.c:40
msgid "regular empty file"
msgstr "reguläre leere Datei"

#: lib/file-type.c:40
msgid "regular file"
msgstr "reguläre Datei"

#: lib/file-type.c:43
msgid "directory"
msgstr "Verzeichnis"

#: lib/file-type.c:46
msgid "symbolic link"
msgstr "symbolische Verknüpfung"

#: lib/file-type.c:52
msgid "message queue"
msgstr "Nachrichtenwarteschlange"

#: lib/file-type.c:55
msgid "semaphore"
msgstr "Semaphor"

#: lib/file-type.c:58
msgid "shared memory object"
msgstr "Objekt gemeinsamen Speichers"

#: lib/file-type.c:61
msgid "typed memory object"
msgstr "Objekt getypten Speichers"

#: lib/file-type.c:66
msgid "block special file"
msgstr "blockorientierte Spezialdatei"

#: lib/file-type.c:69
msgid "character special file"
msgstr "zeichenorientierte Spezialdatei"

#: lib/file-type.c:72
msgid "contiguous data"
msgstr "zusammenhängende Daten"

#: lib/file-type.c:75
msgid "fifo"
msgstr "FIFO"

#: lib/file-type.c:78
msgid "door"
msgstr "Tür"

#: lib/file-type.c:81
msgid "multiplexed block special file"
msgstr "gemultiplexte blockorientierte Spezialdatei"

#: lib/file-type.c:84
msgid "multiplexed character special file"
msgstr "gemultiplexte zeichenorientierte Spezialdatei"

#: lib/file-type.c:87
msgid "multiplexed file"
msgstr "gemultiplexte Datei"

#: lib/file-type.c:90
msgid "named file"
msgstr "benannte Datei"

#: lib/file-type.c:93
msgid "network special file"
msgstr "netzwerkbezogene Spezialdatei"

#: lib/file-type.c:96
msgid "migrated file with data"
msgstr "migrierte Datei mit Daten"

#: lib/file-type.c:99
msgid "migrated file without data"
msgstr "migrierte Datei ohne Daten"

#: lib/file-type.c:102
msgid "port"
msgstr "Anschluss"

#: lib/file-type.c:105
msgid "socket"
msgstr "Socket"

#: lib/file-type.c:108
msgid "whiteout"
msgstr "Überblendung"

#: lib/file-type.c:110
msgid "weird file"
msgstr "merkwürdige Datei"

#: lib/gai_strerror.c:57
msgid "Address family for hostname not supported"
msgstr "Adressfamilie für Hostnamen nicht unterstützt"

#: lib/gai_strerror.c:58
msgid "Temporary failure in name resolution"
msgstr "Temporäre Störung der Namensauflösung"

#: lib/gai_strerror.c:59
msgid "Bad value for ai_flags"
msgstr "Ungültiger Wert für ai_flags"

#: lib/gai_strerror.c:60
msgid "Non-recoverable failure in name resolution"
msgstr "Nicht zu umgehende Störung der Namensauflösung"

#: lib/gai_strerror.c:61
msgid "ai_family not supported"
msgstr "ai_family nicht unterstützt"

#: lib/gai_strerror.c:62
msgid "Memory allocation failure"
msgstr "Speicherallokationsfehler"

#: lib/gai_strerror.c:63
msgid "No address associated with hostname"
msgstr "Keine Adresse mit Hostnamen verbunden"

#: lib/gai_strerror.c:64
msgid "Name or service not known"
msgstr "Name oder Service unbekannt"

#: lib/gai_strerror.c:65
msgid "Servname not supported for ai_socktype"
msgstr "Servname nicht unterstützt für ai_socktype"

#: lib/gai_strerror.c:66
msgid "ai_socktype not supported"
msgstr "ai_socktype nicht unterstützt"

#: lib/gai_strerror.c:67
msgid "System error"
msgstr "Systemfehler"

#: lib/gai_strerror.c:68
msgid "Argument buffer too small"
msgstr "Argumentpuffer zu klein"

#: lib/gai_strerror.c:70
msgid "Processing request in progress"
msgstr "Verarbeitungsanfrage in Bearbeitung"

#: lib/gai_strerror.c:71
msgid "Request canceled"
msgstr "Anfrage abgebrochen"

#: lib/gai_strerror.c:72
msgid "Request not canceled"
msgstr "Anfrage nicht abgebrochen"

#: lib/gai_strerror.c:73
msgid "All requests done"
msgstr "Alle Anfragen erledigt"

#: lib/gai_strerror.c:74
msgid "Interrupted by a signal"
msgstr "Durch Signal unterbrochen"

#: lib/gai_strerror.c:75
msgid "Parameter string not correctly encoded"
msgstr "Parameterzeichenkette nicht korrekt kodiert"

#: lib/gai_strerror.c:87
msgid "Unknown error"
msgstr "Unbekannter Fehler"

#: lib/getopt.c:278
#, c-format
msgid "%s: option '%s%s' is ambiguous\n"
msgstr "%s: Option »%s%s« ist mehrdeutig\n"

#: lib/getopt.c:284
#, c-format
msgid "%s: option '%s%s' is ambiguous; possibilities:"
msgstr "%s: Option »%s%s« ist mehrdeutig; Möglichkeiten:"

#: lib/getopt.c:319
#, c-format
msgid "%s: unrecognized option '%s%s'\n"
msgstr "%s: unbekannte Option »%s%s«\n"

#: lib/getopt.c:345
#, c-format
msgid "%s: option '%s%s' doesn't allow an argument\n"
msgstr "%s: Option »%s%s« erlaubt kein Argument\n"

#: lib/getopt.c:360
#, c-format
msgid "%s: option '%s%s' requires an argument\n"
msgstr "%s: Option »%s%s« erfordert ein Argument\n"

#: lib/getopt.c:621
#, c-format
msgid "%s: invalid option -- '%c'\n"
msgstr "%s: ungültige Option -- »%c«\n"

#: lib/getopt.c:636 lib/getopt.c:682
#, c-format
msgid "%s: option requires an argument -- '%c'\n"
msgstr "%s: Option erfordert ein Argument -- »%c«\n"

#: lib/javacomp.c:150 lib/javacomp.c:174 lib/javacomp.c:200
msgid "invalid source_version argument to compile_java_class"
msgstr "ungültiges Argument »source_version« für »compile_java_class«"

#: lib/javacomp.c:221 lib/javacomp.c:252
msgid "invalid target_version argument to compile_java_class"
msgstr "ungültiges Argument »target_version« für »compile_java_class«"

#: lib/javacomp.c:579
#, c-format
msgid "failed to create \"%s\""
msgstr "Datei »%s« konnte nicht erzeugt werden"

#: lib/javacomp.c:586
#, c-format
msgid "error while writing \"%s\" file"
msgstr "Fehler beim Schreiben der Datei »%s«"

#: lib/javacomp.c:2436
msgid "Java compiler not found, try installing gcj or set $JAVAC"
msgstr "Java-Compiler nicht gefunden, versuchen Sie, das Paket »gcj« zu installieren oder setzen Sie $JAVAC"

#: lib/javaexec.c:417
msgid "Java virtual machine not found, try installing gij or set $JAVA"
msgstr "Virtuelle Maschine für Java nicht gefunden, versuchen Sie, das Paket »gij« zu installieren oder setzen Sie $JAVA"

#: lib/javaversion.c:86
#, c-format
msgid "%s subprocess I/O error"
msgstr "%s-Unterprozess-E/A-Fehler"

#: lib/mkdir-p.c:162
#, c-format
msgid "cannot stat %s"
msgstr "Dateieigenschaften für »%s« konnten nicht bestimmt werden"

#: lib/mkdir-p.c:190
#, c-format
msgid "cannot change permissions of %s"
msgstr "Zugriffsrechte von »%s« konnten nicht geändert werden"

#: lib/mkdir-p.c:200
#, c-format
msgid "cannot create directory %s"
msgstr "Verzeichnis »%s« konnte nicht angelegt werden"

#: lib/obstack.c:338 lib/obstack.c:340 lib/xalloc-die.c:34 lib/xsetenv.c:37
msgid "memory exhausted"
msgstr "Zu wenig Speicher vorhanden"

#: lib/openat-die.c:38
msgid "unable to record current working directory"
msgstr "aktuelles Arbeitsverzeichnis konnte nicht bestimmt werden"

#: lib/openat-die.c:57
msgid "failed to return to initial working directory"
msgstr "es konnte nicht ins ursprüngliche Arbeitsverzeichnis zurückgekehrt werden"

#: lib/pagealign_alloc.c:137
msgid "Failed to open /dev/zero for read"
msgstr "Das Gerät »/dev/zero« konnte nicht zum Lesen geöffnet werden"

#: lib/pipe-filter-gi.c:152
msgid "creation of reading thread failed"
msgstr "Erstellen des Lese-Threads fehlgeschlagen"

#: lib/pipe-filter-gi.c:257 lib/pipe-filter-ii.c:419
#, c-format
msgid "cannot set up nonblocking I/O to %s subprocess"
msgstr "Nicht-blockierendes I/O zu Teilprozess »%s« konnte nicht hergestellt werden"

#: lib/pipe-filter-gi.c:335 lib/pipe-filter-ii.c:457
#, c-format
msgid "communication with %s subprocess failed"
msgstr "Kommunikation mit Teilprozess »%s« fehlgeschlagen"

#: lib/pipe-filter-gi.c:365 lib/pipe-filter-ii.c:345 lib/pipe-filter-ii.c:502
#, c-format
msgid "write to %s subprocess failed"
msgstr "Schreiben zu Teilprozess »%s« fehlgeschlagen"

#: lib/pipe-filter-gi.c:405 lib/pipe-filter-ii.c:366 lib/pipe-filter-ii.c:545
#, c-format
msgid "read from %s subprocess failed"
msgstr "Lesen von Teilprozess »%s« fehlgeschlagen"

#: lib/pipe-filter-gi.c:458
#, c-format
msgid "subprocess %s terminated with exit code %d"
msgstr "Teilprozess »%s« beendet mit Code %d"

#: lib/pipe-filter-ii.c:313
msgid "creation of threads failed"
msgstr "Erstellen von Threads fehlgeschlagen"

#: lib/pipe-filter-ii.c:577
#, c-format
msgid "%s subprocess terminated with exit code %d"
msgstr "Teilprozess »%s« wurde mit Code %d beendet"

#. TRANSLATORS:
#. Get translations for open and closing quotation marks.
#. The message catalog should translate "`" to a left
#. quotation mark suitable for the locale, and similarly for
#. "'".  For example, a French Unicode local should translate
#. these to U+00AB (LEFT-POINTING DOUBLE ANGLE
#. QUOTATION MARK), and U+00BB (RIGHT-POINTING DOUBLE ANGLE
#. QUOTATION MARK), respectively.
#.
#. If the catalog has no translation, we will try to
#. use Unicode U+2018 (LEFT SINGLE QUOTATION MARK) and
#. Unicode U+2019 (RIGHT SINGLE QUOTATION MARK).  If the
#. current locale is not Unicode, locale_quoting_style
#. will quote 'like this', and clocale_quoting_style will
#. quote "like this".  You should always include translations
#. for "`" and "'" even if U+2018 and U+2019 are appropriate
#. for your locale.
#.
#. If you don't know what to put here, please see
#. <https://en.wikipedia.org/wiki/Quotation_marks_in_other_languages>
#. and use glyphs suitable for your language.
#: lib/quotearg.c:362
msgid "`"
msgstr "»"

#: lib/quotearg.c:363
msgid "'"
msgstr "«"

#: lib/regcomp.c:135
msgid "Success"
msgstr "Erfolg"

#: lib/regcomp.c:138
msgid "No match"
msgstr "Keine Übereinstimmung"

#: lib/regcomp.c:141
msgid "Invalid regular expression"
msgstr "Ungültiger regulärer Ausdruck"

#: lib/regcomp.c:144
msgid "Invalid collation character"
msgstr "Ungültiges Sortierungszeichen"

#: lib/regcomp.c:147
msgid "Invalid character class name"
msgstr "Ungültiger Name für Zeichenklasse"

#: lib/regcomp.c:150
msgid "Trailing backslash"
msgstr "Unerwarteter Backslash am Ende"

#: lib/regcomp.c:153
msgid "Invalid back reference"
msgstr "Ungültige Rückreferenz"

#: lib/regcomp.c:156
msgid "Unmatched [, [^, [:, [., or [="
msgstr "Gegenstück zu [, [^, [:, [. oder [= fehlt"

#: lib/regcomp.c:159
msgid "Unmatched ( or \\("
msgstr "Gegenstück zu ( oder \\( fehlt"

#: lib/regcomp.c:162
msgid "Unmatched \\{"
msgstr "Gegenstück zu \\{ fehlt"

#: lib/regcomp.c:165
msgid "Invalid content of \\{\\}"
msgstr "Ungültiger Inhalt in \\{\\}"

#: lib/regcomp.c:168
msgid "Invalid range end"
msgstr "Ungültiges Bereichsende"

#: lib/regcomp.c:171
msgid "Memory exhausted"
msgstr "Zu wenig Speicher vorhanden"

#: lib/regcomp.c:174
msgid "Invalid preceding regular expression"
msgstr "Ungültiger vorhergehender regulärer Ausdruck"

#: lib/regcomp.c:177
msgid "Premature end of regular expression"
msgstr "Vorzeitiges Ende des regulären Ausdrucks"

#: lib/regcomp.c:180
msgid "Regular expression too big"
msgstr "Der reguläre Ausdruck ist zu groß"

#: lib/regcomp.c:183
msgid "Unmatched ) or \\)"
msgstr "Gegenstück zu ) oder \\) fehlt"

#: lib/regcomp.c:676
msgid "No previous regular expression"
msgstr "Kein vorhergehender regulärer Ausdruck"

#. TRANSLATORS: A regular expression testing for an affirmative answer
#. (english: "yes").  Testing the first character may be sufficient.
#. Take care to consider upper and lower case.
#. To enquire the regular expression that your system uses for this
#. purpose, you can use the command
#. locale -k LC_MESSAGES | grep '^yesexpr='
#: lib/rpmatch.c:150
msgid "^[yY]"
msgstr "^[jJyY]"

#. TRANSLATORS: A regular expression testing for a negative answer
#. (english: "no").  Testing the first character may be sufficient.
#. Take care to consider upper and lower case.
#. To enquire the regular expression that your system uses for this
#. purpose, you can use the command
#. locale -k LC_MESSAGES | grep '^noexpr='
#: lib/rpmatch.c:163
msgid "^[nN]"
msgstr "^[nN]"

#: lib/set-acl.c:46
#, c-format
msgid "setting permissions for %s"
msgstr "Zugriffsberechtigungen von »%s« werden festgelegt"

#: lib/siglist.h:31
msgid "Hangup"
msgstr "Aufhängen"

#: lib/siglist.h:34
msgid "Interrupt"
msgstr "Unterbrechung"

#: lib/siglist.h:37
msgid "Quit"
msgstr "Beendet"

#: lib/siglist.h:40
msgid "Illegal instruction"
msgstr "Ungültiger Maschinenbefehl"

#: lib/siglist.h:43
msgid "Trace/breakpoint trap"
msgstr "Trace-/Breakpoint-Falle"

#: lib/siglist.h:46
msgid "Aborted"
msgstr "Abgebrochen"

#: lib/siglist.h:49
msgid "Floating point exception"
msgstr "Gleitkomma-Ausnahme"

#: lib/siglist.h:52
msgid "Killed"
msgstr "Getötet"

#: lib/siglist.h:55
msgid "Bus error"
msgstr "Busfehler"

#: lib/siglist.h:58
msgid "Segmentation fault"
msgstr "Speicherzugriffsfehler"

#: lib/siglist.h:61
msgid "Broken pipe"
msgstr "Unterbrochene Weiterleitung"

#: lib/siglist.h:64
msgid "Alarm clock"
msgstr "Wecker"

#: lib/siglist.h:67
msgid "Terminated"
msgstr "Terminiert"

#: lib/siglist.h:70
msgid "Urgent I/O condition"
msgstr "Dringende I/O-Bedingung"

#: lib/siglist.h:73
msgid "Stopped (signal)"
msgstr "Gestoppt (Signal)"

#: lib/siglist.h:76
msgid "Stopped"
msgstr "Gestoppt"

#: lib/siglist.h:79
msgid "Continued"
msgstr "Fortgesetzt"

#: lib/siglist.h:82
msgid "Child exited"
msgstr "Kindprozess beendet"

#: lib/siglist.h:85
msgid "Stopped (tty input)"
msgstr "Gestoppt (tty-Eingabe)"

#: lib/siglist.h:88
msgid "Stopped (tty output)"
msgstr "Gestoppt (tty-Ausgabe)"

#: lib/siglist.h:91
msgid "I/O possible"
msgstr "I/O möglich"

#: lib/siglist.h:94
msgid "CPU time limit exceeded"
msgstr "CPU-Zeitbegrenzung überschritten"

#: lib/siglist.h:97
msgid "File size limit exceeded"
msgstr "Dateigrößenbegrenzung überschritten"

#: lib/siglist.h:100
msgid "Virtual timer expired"
msgstr "Virtueller Zeitgeber abgelaufen"

#: lib/siglist.h:103
msgid "Profiling timer expired"
msgstr "Zeitmesser zur Leistungsmessung abgelaufen"

#: lib/siglist.h:106
msgid "Window changed"
msgstr "Fenster geändert"

#: lib/siglist.h:109
msgid "User defined signal 1"
msgstr "Benutzerdefiniertes Signal 1"

#: lib/siglist.h:112
msgid "User defined signal 2"
msgstr "Benutzerdefiniertes Signal 2"

#: lib/siglist.h:117
msgid "EMT trap"
msgstr "EMT-Falle"

#: lib/siglist.h:120
msgid "Bad system call"
msgstr "Fehlerhafter Systemaufruf"

#: lib/siglist.h:123
msgid "Stack fault"
msgstr "Stapelfehler"

#: lib/siglist.h:126
msgid "Information request"
msgstr "Informationsanfrage"

#: lib/siglist.h:128
msgid "Power failure"
msgstr "Stromausfall"

#: lib/siglist.h:131
msgid "Resource lost"
msgstr "Ressource verloren"

#: lib/sigpipe-die.c:37
msgid "error writing to a closed pipe or socket"
msgstr "Fehler beim Schreiben in geschlossene Pipe oder Socket"

#: lib/spawn-pipe.c:141 lib/spawn-pipe.c:144 lib/spawn-pipe.c:265
#: lib/spawn-pipe.c:268
msgid "cannot create pipe"
msgstr "Pipe konnte nicht erzeugt werden"

#: lib/strsignal.c:114
#, c-format
msgid "Real-time signal %d"
msgstr "Echtzeitsignal %d"

#: lib/strsignal.c:118
#, c-format
msgid "Unknown signal %d"
msgstr "Unbekanntes Signal %d"

#: lib/timevar.c:316
msgid "Execution times (seconds)"
msgstr "Ausführungszeiten (in Sekunden)"

#: lib/timevar.c:318
msgid "CPU user"
msgstr "CPU Anwendung"

#: lib/timevar.c:318
msgid "CPU system"
msgstr "CPU System"

#: lib/timevar.c:318
msgid "wall clock"
msgstr "Vergangene Zeit"

#: lib/unicodeio.c:102
msgid "iconv function not usable"
msgstr "iconv-Funktion nicht benutzbar"

#: lib/unicodeio.c:104
msgid "iconv function not available"
msgstr "iconv-Funktion nicht verfügbar"

#: lib/unicodeio.c:111
msgid "character out of range"
msgstr "Zeichen außerhalb erlaubter Grenzen"

#: lib/unicodeio.c:181
#, c-format
msgid "cannot convert U+%04X to local character set"
msgstr "das Zeichen U+%04X konnte nicht in lokalen Zeichensatz konvertiert werden"

#: lib/unicodeio.c:183
#, c-format
msgid "cannot convert U+%04X to local character set: %s"
msgstr "das Zeichen U+%04X konnte nicht in lokalen Zeichensatz konvertiert werden: %s"

#: lib/userspec.c:106
msgid "invalid user"
msgstr "ungültiger Benutzername"

#: lib/userspec.c:107
msgid "invalid group"
msgstr "ungültiger Gruppenname"

#: lib/userspec.c:108
msgid "invalid spec"
msgstr "ungültige Angabe"

#: lib/verror.c:73
msgid "unable to display error message"
msgstr "Fehlermeldung konnte nicht angezeigt werden"

#: lib/version-etc.c:73
#, c-format
msgid "Packaged by %s (%s)\n"
msgstr "Paket erstellt von %s (%s)\n"

#: lib/version-etc.c:76
#, c-format
msgid "Packaged by %s\n"
msgstr "Paket erstellt von %s\n"

#. TRANSLATORS: Translate "(C)" to the copyright symbol
#. (C-in-a-circle), if this symbol is available in the user's
#. locale.  Otherwise, do not translate "(C)"; leave it as-is.
#: lib/version-etc.c:83
msgid "(C)"
msgstr "©"

#. TRANSLATORS: The %s placeholder is the web address of the GPL license.
#: lib/version-etc.c:88
#, c-format
msgid ""
"License GPLv3+: GNU GPL version 3 or later <%s>.\n"
"This is free software: you are free to change and redistribute it.\n"
"There is NO WARRANTY, to the extent permitted by law.\n"
msgstr ""
"Lizenz GPLv3+: GNU GPL Version 3 oder höher <%s>.\n"
"Dies ist freie Software: Sie können sie ändern und weitergeben.\n"
"Es gibt keinerlei Garantien, soweit es das Gesetz erlaubt.\n"

#. TRANSLATORS: %s denotes an author name.
#: lib/version-etc.c:105
#, c-format
msgid "Written by %s.\n"
msgstr "Geschrieben von %s.\n"

#. TRANSLATORS: Each %s denotes an author name.
#: lib/version-etc.c:109
#, c-format
msgid "Written by %s and %s.\n"
msgstr "Geschrieben von %s und %s.\n"

#. TRANSLATORS: Each %s denotes an author name.
#: lib/version-etc.c:113
#, c-format
msgid "Written by %s, %s, and %s.\n"
msgstr "Geschrieben von %s, %s und %s.\n"

#. TRANSLATORS: Each %s denotes an author name.
#. You can use line breaks, estimating that each author name occupies
#. ca. 16 screen columns and that a screen line has ca. 80 columns.
#: lib/version-etc.c:120
#, c-format
msgid ""
"Written by %s, %s, %s,\n"
"and %s.\n"
msgstr ""
"Geschrieben von %s, %s, %s\n"
"und %s.\n"

#. TRANSLATORS: Each %s denotes an author name.
#. You can use line breaks, estimating that each author name occupies
#. ca. 16 screen columns and that a screen line has ca. 80 columns.
#: lib/version-etc.c:127
#, c-format
msgid ""
"Written by %s, %s, %s,\n"
"%s, and %s.\n"
msgstr ""
"Geschrieben von %s, %s, %s,\n"
"%s und %s.\n"

#. TRANSLATORS: Each %s denotes an author name.
#. You can use line breaks, estimating that each author name occupies
#. ca. 16 screen columns and that a screen line has ca. 80 columns.
#: lib/version-etc.c:134
#, c-format
msgid ""
"Written by %s, %s, %s,\n"
"%s, %s, and %s.\n"
msgstr ""
"Geschrieben von %s, %s, %s,\n"
"%s, %s und %s.\n"

#. TRANSLATORS: Each %s denotes an author name.
#. You can use line breaks, estimating that each author name occupies
#. ca. 16 screen columns and that a screen line has ca. 80 columns.
#: lib/version-etc.c:142
#, c-format
msgid ""
"Written by %s, %s, %s,\n"
"%s, %s, %s, and %s.\n"
msgstr ""
"Geschrieben von %s, %s, %s,\n"
"%s, %s, %s und %s.\n"

#. TRANSLATORS: Each %s denotes an author name.
#. You can use line breaks, estimating that each author name occupies
#. ca. 16 screen columns and that a screen line has ca. 80 columns.
#: lib/version-etc.c:150
#, c-format
msgid ""
"Written by %s, %s, %s,\n"
"%s, %s, %s, %s,\n"
"and %s.\n"
msgstr ""
"Geschrieben von %s, %s, %s,\n"
"%s, %s, %s, %s\n"
"und %s.\n"

#. TRANSLATORS: Each %s denotes an author name.
#. You can use line breaks, estimating that each author name occupies
#. ca. 16 screen columns and that a screen line has ca. 80 columns.
#: lib/version-etc.c:159
#, c-format
msgid ""
"Written by %s, %s, %s,\n"
"%s, %s, %s, %s,\n"
"%s, and %s.\n"
msgstr ""
"Geschrieben von %s, %s, %s,\n"
"%s, %s, %s, %s,\n"
"%s und %s.\n"

#. TRANSLATORS: Each %s denotes an author name.
#. You can use line breaks, estimating that each author name occupies
#. ca. 16 screen columns and that a screen line has ca. 80 columns.
#: lib/version-etc.c:170
#, c-format
msgid ""
"Written by %s, %s, %s,\n"
"%s, %s, %s, %s,\n"
"%s, %s, and others.\n"
msgstr ""
"Geschrieben von %s, %s, %s,\n"
"%s, %s, %s, %s,\n"
"%s, %s und anderen.\n"

#. TRANSLATORS: The placeholder indicates the bug-reporting address
#. for this package.  Please add _another line_ saying
#. "Report translation bugs to <...>\n" with the address for translation
#. bugs (typically your translation team's web or email address).
#: lib/version-etc.c:249
#, c-format
msgid "Report bugs to: %s\n"
msgstr ""
"Melden Sie Fehler im Programm (auf Englisch) an »%s«.\n"
"Melden Sie Fehler in der Übersetzung an <translation-team-de@lists.sourceforge.net>.\n"

#: lib/version-etc.c:251
#, c-format
msgid "Report %s bugs to: %s\n"
msgstr "Melden Sie %s-Fehler an »%s«\n"

#: lib/version-etc.c:255 lib/version-etc.c:257
#, c-format
msgid "%s home page: <%s>\n"
msgstr "%s-Homepage: %s\n"

#: lib/version-etc.c:260
#, c-format
msgid "General help using GNU software: <%s>\n"
msgstr "Allgemeine Hilfe zur Benutzung von GNU-Software: %s\n"

#: lib/w32spawn.h:49
msgid "_open_osfhandle failed"
msgstr "_open_osfhandle fehlgeschlagen"

#: lib/w32spawn.h:90
#, c-format
msgid "cannot restore fd %d: dup2 failed"
msgstr "Dateideskriptor %d konnte nicht wiederhergestellt werden: dup2 fehlgeschlagen"

#: lib/wait-process.c:231 lib/wait-process.c:263 lib/wait-process.c:325
#, c-format
msgid "%s subprocess"
msgstr "%s-Unterprozess"

#: lib/wait-process.c:282 lib/wait-process.c:354
#, c-format
msgid "%s subprocess got fatal signal %d"
msgstr "%s-Unterprozess bekam tödliches Signal %d"

#: lib/xbinary-io.c:37
msgid "failed to set file descriptor text/binary mode"
msgstr "Dateideskriptor konnte nicht zwischen Text und Binär umgeschaltet werden"

#: lib/xfreopen.c:34
msgid "stdin"
msgstr "Standardeingabe (stdin)"

#: lib/xfreopen.c:35
msgid "stdout"
msgstr "Standardausgabe (stdout)"

#: lib/xfreopen.c:36
msgid "stderr"
msgstr "Standardfehlerausgabe (stderr)"

#: lib/xfreopen.c:37
msgid "unknown stream"
msgstr "Unbekannter Datenstrom"

#: lib/xfreopen.c:38
#, c-format
msgid "failed to reopen %s with mode %s"
msgstr "Erneutes Öffnen von %s mit Modus %s fehlgeschlagen"

#: lib/xmemcoll.c:39
msgid "string comparison failed"
msgstr "Zeichenkettenvergleich fehlgeschlagen"

#: lib/xmemcoll.c:40
msgid "Set LC_ALL='C' to work around the problem."
msgstr "Setzen Sie »LC_ALL=C«, um das Problem zu umgehen."

#: lib/xmemcoll.c:42
#, c-format
msgid "The strings compared were %s and %s."
msgstr "Die verglichenen Zeichenketten waren »%s« und »%s«."

#: lib/xprintf.c:50 lib/xprintf.c:76
msgid "cannot perform formatted output"
msgstr "formatierte Ausgabe konnte nicht durchgeführt werden"

#: lib/xstdopen.c:34
msgid "standard file descriptors"
msgstr "Standarddateideskriptoren"

#: lib/xstrtol-error.c:63
#, c-format
msgid "invalid %s%s argument '%s'"
msgstr "ungültiges %s%s-Argument »%s«"

#: lib/xstrtol-error.c:68
#, c-format
msgid "invalid suffix in %s%s argument '%s'"
msgstr "ungültige Endung in %s%s-Argument »%s«"

#: lib/xstrtol-error.c:72
#, c-format
msgid "%s%s argument '%s' too large"
msgstr "%s%s-Argument »%s« zu groß"

#~ msgid "%.*s: ARGP_HELP_FMT parameter must be positive"
#~ msgstr "%.*s: ARGP_HELP_FMT Parameter muss positiv sein"

#~ msgid "%s: option '--%s' doesn't allow an argument\n"
#~ msgstr "%s: Option »--%s« erlaubt kein Argument\n"

#~ msgid "%s: unrecognized option '--%s'\n"
#~ msgstr "%s: unbekannte Option »--%s«\n"

#~ msgid "%s: option '-W %s' doesn't allow an argument\n"
#~ msgstr "%s: Option »-W %s« erlaubt kein Argument\n"

#~ msgid "%s: option '-W %s' requires an argument\n"
#~ msgstr "%s: Option »-W %s« erfordert ein Argument\n"

#~ msgid "Franc,ois Pinard"
#~ msgstr "François Pinard"

#~ msgid "%s home page: <http://www.gnu.org/software/%s/>\n"
#~ msgstr "Heimatseite von %s: <http://www.gnu.org/software/%s/>.\n"

#~ msgid "%s: illegal option -- %c\n"
#~ msgstr "%s: ungültige Option -- %c\n"

#~ msgid ""
#~ "\n"
#~ "Report bugs to <%s>.\n"
#~ msgstr ""
#~ "\n"
#~ "Melden Sie Fehler (auf Englisch, mit LC_ALL=C) an <%s>.\n"
#~ "Melden Sie Übersetzungsfehler an <translation-team-de@lists.sourceforge.net>\n"

#~ msgid "block size"
#~ msgstr "Blockgröße"
