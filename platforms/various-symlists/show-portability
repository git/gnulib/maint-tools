#!/bin/sh
# Usage: show-portability [--doc] symbol...

LC_ALL=C
export LC_ALL
if test "x$1" = "x--doc"; then
  shift
  # Output a line suitable for the gnulib texinfo documentation.
  # Only the missing ones.
  # Only selected platforms, in a particular order.
  for symbol
  do
    line=
    if ! grep '^'"$symbol"'$' glibc-2.29/* >/dev/null; then
      line=$line', glibc 2.29'
    else
      if ! grep '^'"$symbol"'$' glibc-2.3.6/* >/dev/null; then
        line=$line', glibc 2.3.6'
      fi
    fi
    if ! grep '^'"$symbol"'$' macos-14.5/* >/dev/null; then
      line=$line', macOS 14'
    else
      if ! grep '^'"$symbol"'$' macos-13.6/* >/dev/null; then
        line=$line', macOS 13'
      else
        if ! grep '^'"$symbol"'$' macos-12.7/* >/dev/null; then
          line=$line', macOS 12'
        else
          if ! grep '^'"$symbol"'$' macos-11.7/* >/dev/null; then
            line=$line', macOS 11'
          else
            if ! grep '^'"$symbol"'$' macosx-10.13/* >/dev/null; then
              line=$line', Mac OS X 10.13'
            else
              if ! grep '^'"$symbol"'$' macosx-10.5/* >/dev/null; then
                line=$line', Mac OS X 10.5'
              else
                if ! grep '^'"$symbol"'$' macosx-10.4/* >/dev/null; then
                  line=$line', Mac OS X 10.4'
                else
                  if ! grep '^'"$symbol"'$' macosx-10.3/* >/dev/null; then
                    line=$line', Mac OS X 10.3'
                  fi
                fi
              fi
            fi
          fi
        fi
      fi
    fi
    if ! grep '^'"$symbol"'$' freebsd-14.0/* >/dev/null; then
      line=$line', FreeBSD 14.0'
    else
      if ! grep '^'"$symbol"'$' freebsd-13.0/* >/dev/null; then
        line=$line', FreeBSD 13.0'
      else
        if ! grep '^'"$symbol"'$' freebsd-12.0/* >/dev/null; then
          line=$line', FreeBSD 12.0'
        else
          if ! grep '^'"$symbol"'$' freebsd-11.0/* >/dev/null; then
            line=$line', FreeBSD 11.0'
          else
            if ! grep '^'"$symbol"'$' freebsd-6.4/* >/dev/null; then
              line=$line', FreeBSD 6.4'
            else
              if ! grep '^'"$symbol"'$' freebsd-6.0/* >/dev/null; then
                line=$line', FreeBSD 6.0'
              else
                if ! grep '^'"$symbol"'$' freebsd-5.2.1/* >/dev/null; then
                  line=$line', FreeBSD 5.2.1'
                fi
              fi
            fi
          fi
        fi
      fi
    fi
    if ! grep '^'"$symbol"'$' netbsd-10.0/* >/dev/null; then
      line=$line', NetBSD 10.0'
    else
      if ! grep '^'"$symbol"'$' netbsd-9.3/* >/dev/null; then
        line=$line', NetBSD 9.3'
      else
        if ! grep '^'"$symbol"'$' netbsd-9.0/* >/dev/null; then
          line=$line', NetBSD 9.0'
        else
          if ! grep '^'"$symbol"'$' netbsd-8.0/* >/dev/null; then
            line=$line', NetBSD 8.0'
          else
            if ! grep '^'"$symbol"'$' netbsd-7.1.1/* >/dev/null; then
              line=$line', NetBSD 7.1'
            else
              if ! grep '^'"$symbol"'$' netbsd-5.0/* >/dev/null; then
                line=$line', NetBSD 5.0'
              else
                if ! grep '^'"$symbol"'$' netbsd-3.0/* >/dev/null; then
                  line=$line', NetBSD 3.0'
                fi
              fi
            fi
          fi
        fi
      fi
    fi
    if ! grep '^'"$symbol"'$' openbsd-7.5/* >/dev/null; then
      line=$line', OpenBSD 7.5'
    else
      if ! grep '^'"$symbol"'$' openbsd-6.7/* >/dev/null; then
        line=$line', OpenBSD 6.7'
      else
        if ! grep '^'"$symbol"'$' openbsd-6.0/* >/dev/null; then
          line=$line', OpenBSD 6.0'
        else
          if ! grep '^'"$symbol"'$' openbsd-3.8/* >/dev/null; then
            line=$line', OpenBSD 3.8'
          fi
        fi
      fi
    fi
    if ! grep '^'"$symbol"'$' minix-3.3.0/* >/dev/null; then
      line=$line', Minix 3.3.0'
    else
      if ! grep '^'"$symbol"'$' minix-3.1.8/* >/dev/null; then
        line=$line', Minix 3.1.8'
      fi
    fi
    if ! grep '^'"$symbol"'$' aix-7.3.1/* >/dev/null; then
      line=$line', AIX 7.3.1'
    else
      if ! grep '^'"$symbol"'$' aix-7.2.0/* >/dev/null; then
        line=$line', AIX 7.2'
      else
        if ! grep '^'"$symbol"'$' aix-7.1.0/* >/dev/null; then
          line=$line', AIX 7.1'
        else
          if ! grep '^'"$symbol"'$' aix-6.1.0/* >/dev/null; then
            line=$line', AIX 6.1'
          else
            if ! grep '^'"$symbol"'$' aix-5.3.0a/* >/dev/null; then
              line=$line', AIX 5.3'
            else
              if ! grep '^'"$symbol"'$' aix-5.2.0/* >/dev/null; then
                line=$line', AIX 5.2'
              else
                if ! grep '^'"$symbol"'$' aix-5.1.0/* >/dev/null; then
                  line=$line', AIX 5.1'
                else
                  if ! grep '^'"$symbol"'$' aix-4.3.2/* >/dev/null; then
                    line=$line', AIX 4.3.2'
                  fi
                fi
              fi
            fi
          fi
        fi
      fi
    fi
    if ! grep '^'"$symbol"'$' hpux-11.31/* >/dev/null; then
      line=$line', HP-UX 11.31'
    else
      if ! grep '^'"$symbol"'$' hpux-11.23/* >/dev/null; then
        line=$line', HP-UX 11.23'
      else
        if ! grep '^'"$symbol"'$' hpux-11.11/* >/dev/null; then
          line=$line', HP-UX 11.11'
        else
          if ! grep '^'"$symbol"'$' hpux-11.00/* >/dev/null; then
            line=$line', HP-UX 11.00'
          fi
        fi
      fi
    fi
    #if ! grep '^'"$symbol"'$' irix-6.5/* >/dev/null; then
    #  line=$line', IRIX 6.5'
    #else
    #  if ! grep '^'"$symbol"'$' irix-5.3/* >/dev/null; then
    #    line=$line', IRIX 5.3'
    #  fi
    #fi
    #if ! grep '^'"$symbol"'$' osf1-5.1a/* >/dev/null; then
    #  line=$line', OSF/1 5.1'
    #else
    #  if ! grep '^'"$symbol"'$' osf1-4.0d/* >/dev/null; then
    #    line=$line', OSF/1 4.0'
    #  fi
    #fi
    if ! grep '^'"$symbol"'$' solaris-2.11.4/* >/dev/null; then
      line=$line', Solaris 11.4'
    else
      if ! grep '^'"$symbol"'$' solaris-2.11.3/* >/dev/null; then
        line=$line', Solaris 11.3'
      else
        if ! grep '^'"$symbol"'$' solaris-2.11.0/* >/dev/null; then
          line=$line', Solaris 11.0'
        else
          if ! grep '^'"$symbol"'$' solaris-2.11_2010_11/* >/dev/null; then
            line=$line', Solaris 11 2010-11'
          else
            if ! grep '^'"$symbol"'$' solaris-2.10/* >/dev/null; then
              line=$line', Solaris 10'
            else
              if ! grep '^'"$symbol"'$' solaris-2.9/* >/dev/null; then
                line=$line', Solaris 9'
              else
                if ! grep '^'"$symbol"'$' solaris-2.8/* >/dev/null; then
                  line=$line', Solaris 8'
                else
                  if ! grep '^'"$symbol"'$' solaris-2.7/* >/dev/null; then
                    line=$line', Solaris 7'
                  fi
                fi
              fi
            fi
          fi
        fi
      fi
    fi
    if ! grep '^'"$symbol"'$' cygwin-3.5/* >/dev/null; then
      line=$line', Cygwin 3.5.3'
    else
      if ! grep '^'"$symbol"'$' cygwin-2.9/* >/dev/null; then
        line=$line', Cygwin 2.9'
      else
        if ! grep '^'"$symbol"'$' cygwin-1.7/* >/dev/null; then
          line=$line', Cygwin 1.7.x'
        else
          if ! grep '^'"$symbol"'$' cygwin-1.5/* >/dev/null; then
            line=$line', Cygwin 1.5.x'
          fi
        fi
      fi
    fi
    if ! grep '^'"$symbol"'$' mingw/* >/dev/null; then
      line=$line', mingw'
    fi
    if ! grep '^'"$symbol"'$' msvc14/* >/dev/null; then
      line=$line', MSVC 14'
    else
      if ! grep '^'"$symbol"'$' msvc9/* >/dev/null; then
        line=$line', MSVC 9'
      fi
    fi
    #if ! grep '^'"$symbol"'$' interix-3.5/* >/dev/null; then
    #  line=$line', Interix 3.5'
    #fi
    #if ! grep '^'"$symbol"'$' beos/* >/dev/null; then
    #  line=$line', BeOS'
    #fi
    if ! grep '^'"$symbol"'$' android-9.0/* >/dev/null; then
      line=$line', Android 9.0'
    else
      if ! grep '^'"$symbol"'$' android-4.3/* >/dev/null; then
        line=$line', Android 4.3'
      fi
    fi
    echo "$line" | sed -e 's/^, //' -e 's/$/./'
  done
else
  for symbol
  do
    echo "$symbol"
    {
      for d in */.; do
        os=`echo $d | sed -e 's,/\.$,,'`
        grep '^'"$symbol"'$' $d/* \
          | sed -e 's,^\(.*\)/\./\(.*\):.*,\2                    \1,' \
          | sed -e 's,^libcygwin                    cygwin,libc                    cygwin,' \
                -e 's,^libcmt                    msvc,libc                    msvc,' \
                -e 's,^libmathCommon                    macosx,libc                    macosx,' \
                -e 's,^libsystem_asl                       macosx,libc                    macosx,' \
                -e 's,^libsystem_blocks                    macosx,libc                    macosx,' \
                -e 's,^libsystem_c                         macosx,libc                    macosx,' \
                -e 's,^libsystem_configuration             macosx,libc                    macosx,' \
                -e 's,^libsystem_coreservices              macosx,libc                    macosx,' \
                -e 's,^libsystem_darwin                    macosx,libc                    macosx,' \
                -e 's,^libsystem_dnssd                     macosx,libc                    macosx,' \
                -e 's,^libsystem_info                      macosx,libc                    macosx,' \
                -e 's,^libsystem_kernel                    macosx,libc                    macosx,' \
                -e 's,^libsystem_m                         macosx,libc                    macosx,' \
                -e 's,^libsystem_malloc                    macosx,libc                    macosx,' \
                -e 's,^libsystem_network                   macosx,libc                    macosx,' \
                -e 's,^libsystem_networkextension          macosx,libc                    macosx,' \
                -e 's,^libsystem_notify                    macosx,libc                    macosx,' \
                -e 's,^libsystem_platform                  macosx,libc                    macosx,' \
                -e 's,^libsystem_pthread                   macosx,libc                    macosx,' \
                -e 's,^libsystem_sandbox                   macosx,libc                    macosx,' \
                -e 's,^libsystem_secinit                   macosx,libc                    macosx,' \
                -e 's,^libsystem_symptoms                  macosx,libc                    macosx,' \
                -e 's,^libsystem_trace                     macosx,libc                    macosx,' \
                -e 's,^libSystem                    macos-11,libc                    macos-11,' \
                -e 's,^libdbm                    macos-11,libc                    macos-11,' \
                -e 's,^libdl                    macos-11,libc                    macos-11,' \
                -e 's,^libgcc_s\.1                    macos-11,libc                    macos-11,' \
                -e 's,^libm                    macos-11,libc                    macos-11,' \
                -e 's,^libroot                    beos$,libc                    beos,' \
                -e 's,^libroot                    haiku$,libc                    haiku,'
        grep '^'"$symbol"'$' $d/* >/dev/null || echo $os 1>&3
      done \
        | sed -e 's,^\(....................\) *,\1,' | sort -b -k 2,2 -k 1,1 -u
    } 3> .missing
    missing=`cat .missing`
    rm -f .missing
    if test -n "$missing"; then
      echo "MISSING in         " $missing
    fi
    echo
  done
fi
