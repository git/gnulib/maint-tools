#!/bin/sh

# Copyright (C) 2024 Free Software Foundation, Inc.
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

./foo R
./foo W

./foo RR
./foo RW
./foo WR
./foo WW

./foo RRR
./foo RRW
./foo RWR
./foo RWW
./foo WRR
./foo WRW
./foo WWR
./foo WWW

./foo RRRR
./foo RRRW
./foo RRWR
./foo RRWW
./foo RWRR
./foo RWRW
./foo RWWR
./foo RWWW
./foo WRRR
./foo WRRW
./foo WRWR
./foo WRWW
./foo WWRR
./foo WWRW
./foo WWWR
./foo WWWW

./foo RRRRR
./foo RRRRW
./foo RRRWR
./foo RRRWW
./foo RRWRR
./foo RRWRW
./foo RRWWR
./foo RRWWW
./foo RWRRR
./foo RWRRW
./foo RWRWR
./foo RWRWW
./foo RWWRR
./foo RWWRW
./foo RWWWR
./foo RWWWW
./foo WRRRR
./foo WRRRW
./foo WRRWR
./foo WRRWW
./foo WRWRR
./foo WRWRW
./foo WRWWR
./foo WRWWW
./foo WWRRR
./foo WWRRW
./foo WWRWR
./foo WWRWW
./foo WWWRR
./foo WWWRW
./foo WWWWR
./foo WWWWW
