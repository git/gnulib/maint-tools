(DR) Collect pointers to documentation and references
-----------------------------------------------------

https://wiki.qemu.org/Documentation/Platforms/m68k
https://www.kernel.org/doc/html/v6.6/admin-guide/kernel-parameters.html

(QV) Choose a QEMU version
--------------------------

Use qemu-8.2.0 (This version fixes the 'long double' operations.)
% PATH=$HOME/inst-qemu/8.2.0/bin:$PATH

(DL) Download an installation CD-ROM or DVD image
-------------------------------------------------

Download: https://dl.t2sde.org/binary/2023/t2-23.12-m68k-minimal-desktop-gcc-glibc-68020.iso

(XK) Extract the kernel from the CD-ROM or DVD image
----------------------------------------------------

% sudo mount -r -t iso9660 -o loop t2-23.12-m68k-minimal-desktop-gcc-glibc-68020.iso /mnt
% mkdir boot
% cp -a /mnt/boot/* boot/
% sudo umount /mnt

(CD) Create an empty disk image to be used by the virtual machine
-----------------------------------------------------------------

% qemu-img create -f qcow2 t2sde.qcow2 10G

(MA) Choose the machine arguments
---------------------------------

% machine_args="-M q800 -m 256"

(DI) Choose the disk arguments
------------------------------

% disk_args="-drive file=t2sde.qcow2,format=qcow2,id=hd0"

(NW) Choose the network arguments
---------------------------------

Either
  % net_args=""
or
  % net_args="-net nic,model=dp83932 -net user"
works. Each provides an ethernet interface:
(qemu) info network
hub 0
 \ hub0port1: #net110: index=0,type=user,net=10.0.2.0,restrict=off
 \ hub0port0: dp8393x.0: index=0,type=nic,model=dp8393x,macaddr=08:00:07:12:34:56

(DV) Choose the display/video arguments
---------------------------------------

% display_args="-nographic"

(B1) Boot from the CD/DVD
-------------------------

% common_args="$machine_args $disk_args $net_args $display_args"

Resize the terminal to 80x24.

Either
  % qemu-system-m68k $common_args \
      -kernel boot/vmlinux-6.6.3-t2 -initrd boot/initrd-6.6.3-t2 -append "console=ttyS0,115200" \
      -cdrom t2-23.12-m68k-minimal-desktop-gcc-glibc-68020.iso
or
  % qemu-system-m68k $common_args \
      -kernel boot/vmlinux-6.6.3-t2 -initrd boot/initrd-6.6.3-t2 -append "console=ttyS0,115200" \
      -drive file=t2-23.12-m68k-minimal-desktop-gcc-glibc-68020.iso,if=scsi,bus=0,unit=2,media=cdrom
works; the CD-ROM device is:
(qemu) info block
...
scsi0-cd2 (#block364): t2-23.12-m68k-minimal-desktop-gcc-glibc-68020.iso (raw, read-only)
    Attached to:      /machine/esp/scsi.0/legacy[2]
    Removable device: not locked, tray closed
    Cache mode:       writeback

Here, the '-append "console=..."' option is needed so that output gets directed
to the terminal emulator.

(IN) Perform the steps of the installer
---------------------------------------

Serial terminal: <Enter> or console
# install
Partition:
  fdisk
  n p 1 <Enter> <Enter>
  w
On /dev/sda1: Create filesystem of type ext3 [why not ext4?] with mount point /
Install the system
Start gasgui Package Manager
Full install (all packages).
Keyboard: us
Root password: ********
Time zone: Europe/Berlin
Locale: en_US.UTF-8
Network configuration: Configure eth0 through DHCP. <Back>.
SSH daemon configuration: <Back>.
System init configuration: Default runlevel 3. <Back>.
Then: <Exit>

(B2) Boot from the installed disk
---------------------------------

This needs a -kernel option, since no bootloader is installed.
% qemu-system-m68k $common_args
does not boot:
--------------------------------------------------------------------------------
qemu-system-m68k: could not load MacROM 'MacROM.bin'
--------------------------------------------------------------------------------

So, use:
% qemu-system-m68k $common_args -kernel boot/vmlinux-6.6.3-t2 -initrd boot/initrd-6.6.3-t2 -append "root=/dev/sda1 console=ttyS0,115200"

This boots fine. Now would be the time to switch to a graphic display,
but it does not work:
  - Typing into the window has no effect.

# useradd -d /home/MY_USER_NAME -m -U -u 1000 -G wheel MY_USER_NAME
# passwd MY_USER_NAME
New password: ********

(PK) Install packages
---------------------

Nothing to do. If some packages are needed that are not on the CD-ROM,
they can be compiled locally through
# cd /usr/src/t2-src
# svn update
# scripts/Emerge-Pkg -f PACKAGE
But that takes a long time.
