#!/bin/sh
. ../init.sh
case `autoconf --version | sed -e 's/^[^0-9]*//' -e 1q` in
  2.72) ;;
  *) echo "Skipping test: Autoconf version 2.72 is needed for this test." >&2; exit 77 ;;
esac
do_import_test emacs-20240101 . "--conditional-dependencies --import --no-changelog --no-vc-files --gnu-make --makefile-name=gnulib.mk.in `echo '
  --avoid=btowc
  --avoid=chmod
  --avoid=close
  --avoid=crypto/af_alg
  --avoid=dup
  --avoid=fchdir
  --avoid=fstat
  --avoid=iswblank
  --avoid=iswctype
  --avoid=iswdigit
  --avoid=iswxdigit
  --avoid=langinfo
  --avoid=lock
  --avoid=mbrtowc
  --avoid=mbsinit
  --avoid=memchr
  --avoid=mkdir
  --avoid=msvc-inval
  --avoid=msvc-nothrow
  --avoid=nl_langinfo
  --avoid=openat-die
  --avoid=opendir
  --avoid=pthread-h
  --avoid=raise
  --avoid=save-cwd
  --avoid=select
  --avoid=setenv
  --avoid=sigprocmask
  --avoid=stat
  --avoid=stdarg
  --avoid=threadlib
  --avoid=tzset
  --avoid=unsetenv
  --avoid=utime
  --avoid=utime-h
  --avoid=wchar
  --avoid=wcrtomb
  --avoid=wctype
  --avoid=wctype-h
  alignasof
  alloca-opt
  binary-io
  boot-time
  byteswap
  c-ctype
  c-strcase
  canonicalize-lgpl
  careadlinkat
  close-stream
  copy-file-range
  count-leading-zeros
  count-one-bits
  count-trailing-zeros
  crypto/md5
  crypto/md5-buffer
  crypto/sha1-buffer
  crypto/sha256-buffer
  crypto/sha512-buffer
  d-type
  diffseq
  double-slash-root
  dtoastr
  dtotimespec
  dup2
  environ
  execinfo
  faccessat
  fchmodat
  fcntl
  fcntl-h
  fdopendir
  file-has-acl
  filemode
  filename
  filevercmp
  flexmember
  fpieee
  free-posix
  fstatat
  fsusage
  fsync
  futimens
  getline
  getloadavg
  getopt-gnu
  getrandom
  gettime
  gettimeofday
  gitlog-to-changelog
  ieee754-h
  ignore-value
  intprops
  largefile
  libgmp
  lstat
  manywarnings
  memmem-simple
  mempcpy
  memrchr
  memset_explicit
  minmax
  mkostemp
  mktime
  nanosleep
  nproc
  nstrftime
  pathmax
  pipe2
  pselect
  pthread_sigmask
  qcopy-acl
  readlink
  readlinkat
  regex
  sig2str
  sigdescr_np
  socklen
  stat-time
  std-gnu11
  stdbool
  stdckdint
  stddef
  stdio
  stpcpy
  strnlen
  strnlen
  strtoimax
  symlink
  sys_stat
  sys_time
  tempname
  time-h
  time_r
  time_rz
  timegm
  timer-time
  timespec-add
  timespec-sub
  update-copyright
  unlocked-io
  utimensat
  vla
  warnings
  year2038
'`"
