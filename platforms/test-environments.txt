Test environments
=================

This page explains how to get access to environments of various platforms,
suitable for testing Gnulib.


Testing specific operating systems
==================================

The compile farm
----------------

The compile farm (formerly known as GCC compile farm)
<https://portal.cfarm.net/>
offers developer accounts for Free Software contributors.
After obtaining an account, you have ssh access to machines
with the following OSes:
  - GNU/Linux
  - macOS
  - AIX
  - Solaris


Virtual machines
----------------

For the following OSes, you can install a virtual machine:
  - GNU/Hurd
  - GNU/kFreeBSD
  - musl libc (Alpine Linux)
  - FreeBSD, DragonFly BSD
  - NetBSD
  - OpenBSD
  - Minix
  - Haiku
  - Solaris 11.4, Solaris 11 OpenIndiana, Solaris 11 OmniOS

There is an investment of ca. 2 hours up-front to create the virtual machine,
and it takes some amount of disk space (typically something like 10 GB for
each virtual machine).

How to do this:
1. Pick your preferred hypervisor (such as QEMU or VirtualBox).
2. Download an installation CD-ROM or DVD image.
3. Create an empty disk image to be used by the virtual machine.
4. Find the right parameters for the hypervisor, so that it boots from
   the CD/DVD.
5. Perform the steps of the installer.
6. Reboot the virtual machine, this time booting from the virtual disk.
7. Install whatever packages you will need for development (gcc, g++,
   make, vim, emacs).

As examples, find in the environments/ directory:
  * qemu/x86_64-openbsd73.txt: How to install OpenBSD 7.3 with QEMU.
  * virtualbox/openbsd73.txt: How to install OpenBSD 7.3 with VirtualBox.

Download locations:
  * GNU/Hurd:
    https://cdimage.debian.org/cdimage/ports/12.0/hurd-i386/iso-cd/
  * GNU/kFreeBSD:
    https://cdimage.debian.org/cdimage/archive/7.11.0/kfreebsd-i386/iso-dvd/
    https://cdimage.debian.org/cdimage/archive/7.11.0/kfreebsd-amd64/iso-dvd/


Bring your own hardware
-----------------------

For the following OSes, the simplest approach is to buy some real hardware:
  - Android
    Costs less than $100.
    Configuration details:
    <https://gitlab.com/ghwiki/gnow-how/-/wikis/Platforms/Android>


Testing specific CPUs
=====================

The compile farm
----------------

The compile farm (formerly known as GCC compile farm)
<https://portal.cfarm.net/>
offers developer accounts for Free Software contributors.
After obtaining an account, you have ssh access to machines
with glibc and the following CPUs:
  - arm64
  - loongarch64
  - mips64
  - powerpc64, powerpc64le


Matoro's ISA Sandbox
--------------------

This is a small compile farm focused on glibc machines with
rare CPUs.
<https://static.matoro.tk/isa-sandbox-faq.html>
offers developer accounts.
After obtaining an account, you have ssh access to machines
with glibc and the following CPUs:
  - alpha
  - hppa
  - ia64
  - loongarch64
  - mips64
  - powerpc64, powerpc64le
  - riscv64
  - sparc64


Emulated virtual machines
-------------------------

For testing specific CPUs, such as
  - alpha
  - arm
  - arm64
  - hppa
  - m68k
  - mips, mips64
  - powerpc, powerpc64, powerpc64le
  - riscv64
  - s390x
  - sparc, sparc64
the easiest approach is to use QEMU in system emulation mode.

The how-to is similar to above:
1. Pick your preferred hypervisor (QEMU; for riscv32 you can use tinyemu
   instead).
2. Download an installation CD-ROM or DVD image. For most CPUs, Debian
   provides installable GNU/Linux images.
3. Create an empty disk image to be used by the virtual machine.
4. Find the right parameters for the hypervisor, so that it boots from
   the CD/DVD. This is generally the most challenging part, because
     - for some CPUs the boot loader is not standardized,
     - for hardware devices such as the Ethernet card, you need to find
       a model that QEMU can emulate _and_ that the OS can make use of.
5. Perform the steps of the installer.
6. Reboot the virtual machine without the CD/DVD.
7. Install whatever packages you will need for development (gcc, g++,
   make, vim, emacs).

Specific, detailed instructions for QEMU are in
environments/qemu-environments.txt.

As examples, find in the environments/ directory:
  * qemu/alpha-linux-debian5.txt
  * qemu/alpha-linux-debian12.txt
  * qemu/alpha-linux-t2sde.txt
  * qemu/arm-freebsd12.txt
  * qemu/arm-freebsd14.txt
  * qemu/armel-linux-debian8.txt
  * qemu/armelhf-linux-debian12.txt
  * qemu/armelhf-raspbian.txt
  * qemu/arm64-linux-debian12.txt
  * qemu/arm64-linux-opensuse-a.txt
  * qemu/arm64-linux-opensuse-b.txt
  * qemu/arm64-alpine-3.13.txt
  * qemu/arm64-alpine-3.19.txt
  * qemu/arm64-freebsd132.txt
  * qemu/arm64-netbsd9.txt
  * qemu/arm64-netbsd10.txt
  * qemu/hppa-linux-debian12.txt
  * qemu/hppa-linux-t2sde.txt
  * qemu/hppa-netbsd9.txt
  * qemu/hppa-netbsd10.txt
  * qemu/m68k-linux-debian12.txt
  * qemu/m68k-linux-t2sde.txt
  * qemu/mipseb-linux-debian8.txt
  * qemu/mipsel-linux-debian12.txt
  * qemu/mipsel-linux-debian8.txt
  * qemu/mips64eb-linux-debian8.txt
  * qemu/mips64el-linux-debian8.txt
  * qemu/powerpc-linux-debian12.txt
  * qemu/powerpc-linux-t2sde.txt
  * qemu/powerpc64-linux-debian12.txt
  * qemu/ppc64le-linux-debian12.txt
  * qemu/ppc64le-linux-opensuse-kde.txt
  * qemu/ppc64le-linux-opensuse.txt
  * qemu/ppc64le-alpine.txt
  * qemu/riscv64-linux-fedora28.txt
  * qemu/s390x-linux-debian8.txt
  * qemu/s390x-linux-opensuse.txt
  * qemu/s390x-alpine.txt
  * qemu/sparc-netbsd7.txt
  * qemu/sparc-netbsd10.txt
  * qemu/sparc64-linux-debian9.txt
  * qemu/sparc64-linux-t2sde.txt
  * qemu/sparc64-freebsd12.txt
  * qemu/sparc64-netbsd8.txt
  * qemu/sparc64-netbsd10.txt

Download locations:
  * GNU/Linux and Linux:
    See linux-install-media.txt.

QEMU in "user mode" is not recommended in general, because it has limitations:
  - Catching stack overflow is not supported.
  - ioctl() are not well supported (e.g. functions that deal with ttys).
  - glibc iconv modules are not installed.
  - On alpha: O_CLOEXEC is not well supported (e.g. in dup3, open).
  - On mips: Quiet NaNs behave like signalling NaNs and vice versa.
  - On sparc: Spin lock tests hang.
That is, in the really tricky areas QEMU in "user mode" is less reliable
than QEMU in system emulation mode.


Bring your own hardware
-----------------------

For the following CPUs, it is possible to buy some real hardware:
  - arm
    Raspberry Pi 1 or 2
    <https://en.wikipedia.org/wiki/Raspberry_Pi>
    Costs less than $50.
  - arm64
    Raspberry Pi 3 or newer
    <https://en.wikipedia.org/wiki/Raspberry_Pi>
    Costs less than $100.
  - mips
    Onion Omega2 Pro, costs less than $100.
  - riscv64
    SiFive HF103-000, costs less than $50.
    SiFive HF103-001, costs less than $100.
    StarFive VisionFive 2, costs less than $100.
    BeagleBoard BeagleV, costs less than $200.
