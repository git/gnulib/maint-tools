#!/bin/sh
. ../init.sh
do_import_test gettext-20240101 . "`echo '
  --dir=gettext-tools
  --lib=libgettextlib
  --source-base=gnulib-lib
  --m4-base=gnulib-m4
  --tests-base=gnulib-tests
  --makefile-name=Makefile.gnulib
  --libtool
  --with-tests
  --local-dir=gnulib-local
  --import
  --avoid=fdutimensat-tests
  --avoid=futimens-tests
  --avoid=utime-tests
  --avoid=utimens-tests
  --avoid=utimensat-tests
  --avoid=array-list-tests
  --avoid=linked-list-tests
  --avoid=linkedhash-list-tests
  --avoid=unilbrk/u8-possible-linebreaks-tests
  --avoid=unilbrk/ulc-width-linebreaks-tests
  --avoid=unistr/u8-mbtouc-tests
  --avoid=unistr/u8-mbtouc-unsafe-tests
  --avoid=uniwidth/width-tests

  alloca-opt
  atexit
  attribute
  backupfile
  basename-lgpl
  binary-io
  bison
  bison-i18n
  byteswap
  c-ctype
  c-strcase
  c-strcasestr
  c-strstr
  clean-temp
  closedir
  closeout
  configmake
  copy-file
  csharpcomp
  csharpexec
  error
  error-progname
  execute
  filename
  findprog
  flexmember
  fnmatch
  fopen
  free-posix
  fstrcmp
  full-write
  fwriteerror
  gcd
  getaddrinfo
  getline
  getopt-gnu
  gettext-h
  iconv
  javacomp
  javaexec
  libunistring-optional
  libxml
  localcharset
  locale
  localename
  localtime
  lock
  mem-hash-map
  memchr
  memmove
  memset
  minmax
  mkdir
  noreturn
  obstack
  open
  opendir
  openmp-init
  pipe-filter-ii
  progname
  propername
  read-file
  readdir
  relocatable-prog
  relocatable-script
  setlocale
  sh-filename
  sh-quote
  sigpipe
  sigprocmask
  spawn-pipe
  stdbool
  stdio
  stdlib
  stpcpy
  stpncpy
  strchrnul
  strcspn
  strerror
  string-desc
  strpbrk
  strtol
  strtoul
  supersede
  sys_select
  sys_stat
  sys_time
  trim
  unictype/ctype-space
  unictype/syntax-java-whitespace
  unilbrk/ulc-width-linebreaks
  uniname/uniname
  unistd
  unistr/u8-check
  unistr/u8-mbtouc
  unistr/u8-mbtoucr
  unistr/u8-uctomb
  unistr/u16-mbtouc
  uniwidth/width
  unlocked-io
  unsetenv
  vasprintf
  wait-process
  write
  xalloc
  xconcat-filename
  xerror
  xmalloca
  xmemdup0
  xsetenv
  xstrerror
  xstriconv
  xstriconveh
  xvasprintf

  alloca-opt
  extensions
  gettext-h
  include_next
  locale
  localcharset
  malloc-posix
  mbrtowc
  mbsinit
  multiarch
  setlocale-null
  snippet/arg-nonnull
  snippet/c++defs
  snippet/warn-on-use
  ssize_t
  stdbool
  stddef
  stdint
  stdlib
  streq
  unistd
  verify
  wchar
  wctype-h
  windows-mutex
  windows-once
  windows-recmutex
  windows-rwlock

  gettext-tools-misc
  ansi-c++-opt
  csharpcomp-script
  csharpexec-script
  java
  javacomp-script
  javaexec-script
  manywarnings
  stdint
'`"
