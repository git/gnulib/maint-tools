(DR) Collect pointers to documentation and references
-----------------------------------------------------

https://www.qemu.org/docs/master/system/target-sparc64.html
https://wiki.qemu.org/Documentation/Platforms/SPARC
https://www.kernel.org/doc/html/v6.6/admin-guide/kernel-parameters.html

(QV) Choose a QEMU version
--------------------------

Use qemu-8.1.2
% PATH=$HOME/inst-qemu/8.1.2/bin:$PATH

(DL) Download an installation CD-ROM or DVD image
-------------------------------------------------

Download: https://dl.t2sde.org/binary/2023/t2-23.12-sparc6432-minimal-desktop-gcc-glibc.iso

(XK) Extract the kernel from the CD-ROM or DVD image
----------------------------------------------------

% sudo mount -r -t iso9660 -o loop t2-23.12-sparc6432-minimal-desktop-gcc-glibc.iso /mnt
% mkdir boot
% cp -a /mnt/boot/* boot/
% sudo umount /mnt

(CD) Create an empty disk image to be used by the virtual machine
-----------------------------------------------------------------

% qemu-img create -f qcow2 t2sde.qcow2 10G

(MA) Choose the machine arguments
---------------------------------

% machine_args="-m 2048"
because with just "-m 512" we would later get an error during installation:
--------------------------------------------------------------------------------
Out of memory (oom_kill_allocating_task): Killed process 2023 (mine) total-vm:2328kB, anon-rss:0kB, file-rss:0kB, shmem-rss:0kB, UID:0 pgtables:48kB oom_score_adj:0
Killed
Error while installing package rustc!
--------------------------------------------------------------------------------
and with just "-m 1024" we would later get an error during installation as well:
--------------------------------------------------------------------------------
...
Installing ntfsprogs-2022.5.17 ...
Installing ntp-4.2.8p17 ...
BUG: non-zero pgtables_bytes on freeing mm: 8192
Kernel panic - not syncing: Attempted to kill init! exitcode=0x00000100
CPU: 0 PID: 1 Comm: init Not tainted 6.6.7-t2 #1
Call Trace:
[<0000000000a35ab8>] dump_stack+0x8/0x18
[<0000000000466840>] panic+0x10c/0x370
[<000000000046c57c>] do_exit+0x9fc/0xa20
[<000000000046c73c>] do_group_exit+0x1c/0xa0
[<000000000046c7d4>] sys_exit_group+0x14/0x20
[<0000000000406214>] linux_sparc_syscall32+0x34/0x60
Rebooting in 60 seconds..
BOOTKernel panic - not syncing: Reboot failed!
Rebooting in 60 seconds..
--------------------------------------------------------------------------------

(DI) Choose the disk arguments
------------------------------

% disk_args="-drive file=t2sde.qcow2,format=qcow2,index=0"

(NW) Choose the network arguments
---------------------------------

% net_args=""
This provides an ethernet interface:
(qemu) info network
hub 0
 \ hub0port1: #net109: index=0,type=user,net=10.0.2.0,restrict=off
 \ hub0port0: sunhme.0: index=0,type=nic,model=sunhme,macaddr=52:54:00:12:34:56

(DV) Choose the display/video arguments
---------------------------------------

% display_args="-nographic"

(B1) Boot from the CD/DVD
-------------------------

% common_args="$machine_args $disk_args $net_args $display_args"

Resize the terminal to 80x24.

Either boot directly into Linux:
% qemu-system-sparc64 $common_args \
    -kernel boot/vmlinux-6.6.7-t2 -initrd boot/initrd-6.6.7-t2 \
    -cdrom t2-23.12-sparc6432-minimal-desktop-gcc-glibc.iso
works; the CD-ROM device is:
(qemu) info block
...
ide1-cd0 (#block387): t2-23.12-sparc6432-minimal-desktop-gcc-glibc.iso (raw, read-only)
    Attached to:      /machine/unattached/device[19]
    Removable device: not locked, tray closed
    Cache mode:       writeback

Or boot into Linux via grub:
% qemu-system-sparc64 $common_args \
    -cdrom t2-23.12-sparc6432-minimal-desktop-gcc-glibc.iso \
    -boot d

(IN) Perform the steps of the installer
---------------------------------------

Serial terminal: <Enter> or console
# install
Keyboard: us
Partition: Automatic, classic partitions.
File system on /dev/sda1: ext4
  Proceed anyway: y
Install the system
Start gasgui Package Manager
Full install (all packages).
Keyboard: us
Root password: ********
Time zone: Europe/Berlin
Locale: en_US.UTF-8
Generated /boot/grub/grub.cfg:
  menuentry "T2/Linux" {
    linux /boot/vmlinux-6.6.7-t2 root=/dev/disk/by-uuid/6191b4f7-31be-4b8a-8a8a-a545cb286a8b ro resume=/dev/disk/by-uuid/c5f7d2b5-ff7a-4c09-880e-864b85cc52b4 console=ttyS0 initrd /boot/initrd-6.6.7-t2
  }
Error: grub2-install: error: unknown filesystem.
Therefore: <Continue>
Generated /boot/silo.conf:
  root=/dev/sda1
  image=/boot/vmlinuz-6.6.7-t2
   label=linux root=/dev/sda1 initrd=/boot/initrd-6.6.7-t2 read-only
Again: <Continue>
SILO configuration: <Continue>.
Network configuration: Configure eth0 through DHCP. <Continue>.
SSH daemon configuration: <Continue>.
System init configuration: Default runlevel 3. <Continue>.
Then: <Quit>
# halt

(B2) Boot from the installed disk
---------------------------------

This needs a -kernel option, since no bootloader is installed.
% qemu-system-sparc64 $common_args
does not boot:
--------------------------------------------------------------------------------
OpenBIOS for Sparc64
Configuration device id QEMU version 1 machine id 0
kernel cmdline
CPUs: 1 x SUNW,UltraSPARC-IIi
UUID: 00000000-0000-0000-0000-000000000000
Welcome to OpenBIOS v1.1 built on Mar 7 2023 22:22
  Type 'help' for detailed information
Trying disk:a...
Not a bootable ELF image
Loading a.out image...
Loaded 7680 bytes
entry point is 0x4000
SILO Version 1.4.14

Cannot find /boot/silo.conf (error: 4294967295)

Couldn't load /boot/silo.conf
No config file loaded, you can boot just from this command line
Type [prompath;]part/path_to_image [parameters] on the prompt
E.g. /iommu/sbus/espdma/esp/sd@3,0;4/vmlinux root=/dev/sda4
or 2/vmlinux.live (to load vmlinux.live from 2nd partition of boot disk)
boot:
--------------------------------------------------------------------------------
Likewise with '-boot c'.

So, use:
% qemu-system-sparc64 $common_args -kernel boot/vmlinux-6.6.7-t2 -initrd boot/initrd-6.6.7-t2 -append "root=/dev/sda1"

It boots, but there is no login prompt. There are two possible fixes for this situation:

* Tell the kernel about the console device, so that a 'getty' process gets spawned for it.
  (See also the generated grub.cfg, above.)
  % qemu-system-sparc64 $common_args -kernel boot/vmlinux-6.6.7-t2 -initrd boot/initrd-6.6.7-t2 -append "root=/dev/sda1 console=ttyS0,115200"

* Use a graphic display:
  % display_args="-display gtk -monitor stdio"
  % common_args="$machine_args $disk_args $net_args $display_args"
  % qemu-system-sparc64 $common_args -kernel boot/vmlinux-6.6.7-t2 -initrd boot/initrd-6.6.7-t2 -append "root=/dev/sda1"
  But this mode is rather useless, since typing into the VGA window has
  no effect, and the serial0 window merely displays what the console would
  show.

# useradd -d /home/MY_USER_NAME -m -U -u 1000 -G wheel MY_USER_NAME
# passwd MY_USER_NAME
New password: ********

(PK) Install packages
---------------------

Nothing to do. If some packages are needed that are not on the CD-ROM,
they can be compiled locally through
# cd /usr/src/t2-src
# svn update
# scripts/Emerge-Pkg -f PACKAGE
But that takes a long time.


Notes
-----

During installation, the kernel printed:
--------------------------------------------------------------------------------
rcu: INFO: rcu_sched detected stalls on CPUs/tasks:
rcu:    (detected by 0, t=5259 jiffies, g=429925, q=32 ncpus=1)
rcu: All QSes seen, last rcu_sched kthread activity 5259 (4295315252-4295309993), jiffies_till_next_fqs=1, root ->qsmask 0x0
rcu: rcu_sched kthread starved for 5259 jiffies! g429925 f0x2 RCU_GP_WAIT_FQS(5) ->state=0x0 ->cpu=0
rcu:    Unless rcu_sched kthread gets sufficient CPU time, OOM is now expected behavior.
rcu: RCU grace-period kthread stack dump:
rcu: Stack dump where RCU GP kthread last ran:
CPU: 0 PID: 23 Comm: kcompactd0 Not tainted 6.6.7-t2 #1
TSTATE: 0000000011001603 TPC: 00000000009c5cf8 TNPC: 00000000009c5cfc Y: 00000000    Not tainted
TPC: <memcpy+0x1b8/0x13c0>
g0: 000006000003a650 g1: fffff800019f3ff8 g2: 0000000000000000 g3: 0000000000000038
g4: fffff800040abb80 g5: fffff8007eb7e000 g6: fffff80004120000 g7: 0000000000000940
o0: fffff80002eb1600 o1: fffff800019f36c0 o2: 0000000000000008 o3: 00000000009c5cc0
o4: fffff80002eb0000 o5: 0000000000000000 sp: fffff80004122cd1 ret_pc: 000000000044f8e0
RPC: <copy_highpage+0x20/0x5c>
l0: 0000000000000000 l1: 0000000000000402 l2: 0000000000ba06d0 l3: ffffffffffffffff
l4: 0000000004000000 l5: 0000000000000040 l6: 00000000ffffffff l7: 0000000000000048
i0: 00000600000690c0 i1: 000006000003a608 i2: 0000000000000402 i3: 0000000000000000
i4: fffff8007f807940 i5: fffff800019f2000 i6: fffff80004122d81 i7: 00000000005a9a0c
I7: <folio_copy+0x2c/0x60>
Call Trace:
[<00000000005a9a0c>] folio_copy+0x2c/0x60
[<00000000006233c8>] move_to_new_folio+0xc8/0x300
[<0000000000624270>] migrate_pages_batch.constprop.0+0xc70/0x17e0
[<0000000000626778>] migrate_pages_sync+0x58/0x200
[<00000000006273ec>] migrate_pages+0x46c/0x4c0
[<00000000005b7df4>] compact_zone+0xc14/0xf40
[<00000000005b8164>] proactive_compact_node+0x44/0x120
[<00000000005b872c>] kcompactd+0x16c/0x360
[<000000000048daa8>] kthread+0x128/0x160
[<00000000004060c8>] ret_from_fork+0x1c/0x2c
[<0000000000000000>] 0x0
--------------------------------------------------------------------------------
multiple times.

"gcc" produces 32-bit binaries. "gcc -m64" produces 64-bit binaries.

In 64-bit mode, the compilation of mktime.c crashes:
depbase=`echo mktime.o | sed 's|[^/]*$|.deps/&|;s|\.o$||'`;\
gcc -m64 -DHAVE_CONFIG_H -DEXEEXT=\"\" -DEXEEXT=\"\" -DNO_XMALLOC -DEXEEXT=\"\" -I. -I../../gllib -I..  -DGNULIB_STRICT_CHECKING=1 -I/home/bruno/prefix64/include -Wall -fvisibility=hidden -g -O2 -MT mktime.o -MD -MP -MF $depbase.Tpo -c -o mktime.o ../../gllib/mktime.c &&\
mv -f $depbase.Tpo $depbase.Po
during GIMPLE pass: slp
../../gllib/mktime.c: In function 'ranged_convert':
../../gllib/mktime.c:271:1: internal compiler error: Bus error
  271 | ranged_convert (struct tm *(*convert) (const __time64_t *, struct tm *),
      | ^~~~~~~~~~~~~~
0x1303c37 internal_error(char const*, ...)
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
0xf732fc03 __rt_sigreturn_stub
        ???:0
Please submit a full bug report, with preprocessed source (by using -freport-bug).
Please include the complete backtrace with any bug report.
See <https://gcc.gnu.org/bugs/> for instructions.
make[4]: *** [Makefile:11320: mktime.o] Error 1
